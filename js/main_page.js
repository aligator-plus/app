var current_page = 1;
var current_site_id = -1;

function getToken() {
	var token_json = localStorage.getItem('aligator-plus_token');
	if (!token_json) {
		return null;
	}
	return JSON.parse(token_json);
}
function makeRequest(action, data, callback, block) {
	setLoadingBar(block, true);
	var host = 'https://aligator-plus.herokuapp.com/';
	var url = host + action;

	var request_params = {
		method: 'POST',
		headers: {
  		'Content-type': 'multipart/form-data; charset=UTF-8',
  		'Authorization': 'Token ' + getToken()
 		}
	};
	if (data) {
		request_params['body'] = data;
	}

	fetch(url, request_params)
		.then(response => response.json())
		.then(response => {
			setLoadingBar(block, false);
			callback(response);
		})
		.catch(response => {
			setLoadingBar(block, false);
			console.log('Request error: ', response);
		});
}

function showFilter() {
	var filter = document.getElementsByClassName('filter')[0];
	var shadow = document.getElementsByClassName('shadow-content')[0];
	var button = document.getElementById('filter_show_button');
	if (filter.getAttribute('showed') === 'true') {
		filter.setAttribute('showed', 'false');
		filter.style.left = '-9%';
		shadow.style.opacity = '0';
		shadow.style.display = 'none';
		button.classList.remove('ib_active');
	}
	else {
		loadSites();

		filter.setAttribute('showed', 'true');
		filter.style.left = '8%';
		shadow.style.display = 'block';
		setTimeout(() => shadow.style.opacity = '1', 20);
		button.classList.add('ib_active');
	}
}

function loadSites(response) {
	var help_message = document.getElementsByClassName('help_message')[0];
	if (response) {
		if (response.status === 401) {
			help_message.style.color = 'rgba(255, 0, 0, .8)';
			help_message.innerText = response.response.errorMessages[0];
			return;
		}
		else {
			help_message.style.color = 'rgb(51, 204, 51)';
			help_message.innerText = 'You`ve successfully sibscribed to ' + response.response;
		}
	}
	else {
		help_message.innerText = '';
	}

	var block = document.getElementsByClassName('filter')[0];
	makeRequest('getsites', '', setSites, block);
}
function setSites(response) {
	var list_block = document.getElementsByClassName('sites_list')[0];
	list_block.innerHTML = '';
	for (var i = 0; i < response.length; i++) {
		var list_element = document.createElement('div');
		list_element.classList.add('list-site');

		var name = document.createElement('div');
		name.classList.add('name-site');
		name.innerText = response[i].title;
		name.setAttribute('id', response[i].id);
		name.addEventListener('click', changeCurrentSite);

		var link = document.createElement('a');
		link.classList.add('site');
		link.href = 'http://' + response[i].host;
		link.innerText = response[i].host;

		list_element.appendChild(name);
		list_element.appendChild(link);
		list_block.appendChild(list_element);
	}
}
function changeCurrentSite() {
	var this_id = this.getAttribute('id');
	if (!this_id) {
		return;
	}
	current_page = 1;
	current_site_id = this_id;
	showFilter();
	loadArticles();
}
function addNewSite() {
	var url_field = document.getElementsByClassName('input-add-site')[0];
	if (!url_field.value) {
		return;
	}
	var data = url_field.name + '=' + url_field.value;
	var block = document.getElementsByClassName('filter')[0];
	makeRequest('subscribe', data, loadSites, block);
	loadArticles();
	url_field.value = '';
}

function loadUsername() {
	makeRequest('getusername', '', setUsername, '');
}
function setUsername(response) {
	if (response.status !== 200) {
		setTimeout(() => window.location.href = "./index.html", 100);
		return;
	}
	var username_block = document.getElementsByClassName('username_block')[0];
	var username = response.response.charAt(0).toUpperCase() + response.response.slice(1);
	username_block.innerHTML = 'You are logged in as <span>' + username + '</span>';
}
function loadArticles() {
	var data = 'site[page]=' + current_page;
	if (current_site_id !== -1) {
		data += '&site[id]=' + current_site_id;
	}
	var list_block = document.getElementsByClassName('wrapper')[0];
	list_block.innerHTML = '';
	var block = document.getElementsByClassName('layout')[0];
	makeRequest('getarticles', data, setArticles, block);
}
function loadNextPage() {
	current_page++;
	loadArticles();
}
function setArticles(response) {
	console.log(response);
	if (response.status === 401) {
		setTimeout(() => window.location.href = "./index.html", 100);
		return;
	}
	if (response.length === 0 && current_site_id === -1) {
		var starting_help_message = document.getElementsByClassName('starting_help_message')[0];
		starting_help_message.style.display = 'block';
		var starting_help_arrow_img = document.getElementsByClassName('starting_help_arrow_img')[0];
		starting_help_arrow_img.style.display = 'block';
		setTimeout(() => starting_help_arrow_img.style.opacity = '1', 20);

		return;
	}
	else {
		var starting_help_message = document.getElementsByClassName('starting_help_message')[0];
		starting_help_message.style.display = 'none';
		var starting_help_arrow_img = document.getElementsByClassName('starting_help_arrow_img')[0];
		starting_help_arrow_img.style.display = 'none';
		starting_help_arrow_img.style.opacity = '0';
	}

	var list_block = document.getElementsByClassName('wrapper')[0];
	list_block.innerHTML = '';
	for (var i = 0; i < 10; i++) {
		var list_element = document.createElement('div');
		list_element.classList.add('flex-element');
		list_element.classList.add('article');
			var container = document.createElement('div');
			container.classList.add('flex-container');
			container.classList.add('c');

				var content = document.createElement('div');
				content.classList.add('content');

					var title = document.createElement('p');
					title.innerText = response[i].title;

					var info = document.createElement('div');
					info.classList.add('info');

						var site = document.createElement('a');
						site.classList.add('site');
						site.href = 'http://' + response[i].host;
						site.innerHTML = response[i].host + '<br>';

						var time_date = new Date(response[i].published_at);
						var time_str = time_date.getFullYear() + '-' + (time_date.getMonth()+1) + '-' + time_date.getDate();
						time_str += ' ' + time_date.getHours() + ':' + (time_date.getMinutes()<10?'0':'') + time_date.getMinutes();
						var data = document.createTextNode(time_str);

					info.appendChild(site);
					info.appendChild(data);

					var description = document.createElement('div');
					description.innerHTML = response[i].description;
					description.classList.add('description');

					var link = document.createElement('a');
					link.classList.add('read-more');
					link.href = response[i].link;
					link.innerHTML = 'Read more &raquo';

				content.appendChild(title);
				content.appendChild(info);
				content.appendChild(description);
				content.appendChild(link);

			container.appendChild(content);
		list_element.appendChild(container);

		list_block.appendChild(list_element);
	}
	var next_page_button = document.createElement('button');
	next_page_button.classList.add('next_page_button');
	next_page_button.innerText = 'New page'
	next_page_button.addEventListener('click', loadNextPage);

	list_block.appendChild(next_page_button);
}

function logOut() {
	localStorage.removeItem('aligator-plus_token');
	setTimeout(() => window.location.href = "./index.html", 200);
}
function setLoadingBar(block, display_bool) {
	if (!block) {
		return;
	}
	var visibility = display_bool ? 'visible' : 'hidden';
	var loading_bar_arr = block.getElementsByClassName('loading_bar');
	for (var i = 0; i < loading_bar_arr.length; i++) {
		loading_bar_arr[i].style.visibility = visibility;
	}
}

window.onload = function () {
	document.getElementById('filter_show_button').addEventListener('click', showFilter);
	document.getElementsByClassName('shadow-content')[0].addEventListener('click', showFilter);
	document.getElementsByClassName('button-add-site')[0].addEventListener('click', addNewSite);
	document.getElementById('logout_button').addEventListener('click', logOut);

	loadUsername();
	loadArticles();
}

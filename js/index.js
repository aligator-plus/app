function changeForm() {
	if (this.innerText === 'Log in') {
		var signup_button = document.getElementById('form_registration_button');
		var login_button = document.getElementById('form_login_button');
		if (signup_button.type != 'submit') {
			return;
		}
		signup_button.classList = 'button';
		login_button.classList = 'active';
		var content_r = document.getElementsByClassName('content_r')[0];
		var content_login = document.getElementsByClassName('content_login')[0];
		content_r.style.transform = 'translateX(100vw)';
		content_login.style.display = 'block';
		setTimeout(() => {
			content_login.style.transform = 'translateX(0)';
		}, 50);
		signup_button.type = 'button';
		
		signup_button.addEventListener('click', changeForm);
		setTimeout(() => {
			content_r.style.display = 'none';
		}, 500);

		var inputs = content_r.children;
		for (var i = 0; i < inputs.length; i++) {
			inputs[i].required = false;
		}
		inputs = content_login.children;
		for (var i = 0; i < inputs.length; i++) {
			inputs[i].required = true;
		} 
	}
	else if (this.innerText === 'Sign up') {
		var login_button = document.getElementById('form_login_button');
		var signup_button = document.getElementById('form_registration_button');
		if (login_button.type != 'submit') {
			return;
		}
		login_button.classList = 'button';
		signup_button.classList = 'active';
		var content_login = document.getElementsByClassName('content_login')[0];
		var content_r = document.getElementsByClassName('content_r')[0];
		content_login.style.transform = 'translateX(-100vw)';
		content_r.style.display = 'block';
		setTimeout(() => {
			content_r.style.transform = 'translateX(0)';
		}, 50);
		
		login_button.type = 'button';
		login_button.addEventListener('click', changeForm);
		setTimeout(() => {
			content_login.style.display = 'none';
		}, 500);

		var inputs = content_login.children;
		for (var i = 0; i < inputs.length; i++) {
			inputs[i].required = false;
		}
		inputs = content_r.children;
		for (var i = 0; i < inputs.length; i++) {
			inputs[i].required = true;
		} 
	}
	this.removeEventListener('click', changeForm);
	setTimeout(() => this.type = 'submit', 500);
}

function getFormDataString(form_block) {
	var fields_array = [];
	for (var i = 0; i < form_block.children.length; i++) {
		var field = form_block.children[i];
		if (field.nodeName != 'INPUT') {
			continue;
		}
		fields_array.push(field.name + "=" + field.value);
	}
	return fields_array.join("&");
}

function submitForm() {
	var action, submit_form_name, callback;
	if (document.getElementById('form_registration_button').type === 'submit') {
		action = 'signup';
		submit_form_name = 'content_r';
		callback = signUpResult;
	}
	else {
		action = 'login';
		submit_form_name = 'content_login';
		callback = logInResult;
	}
	
	var submit_form = document.getElementsByClassName(submit_form_name)[0];
	var data = getFormDataString(submit_form);
	if (!data) {
		return;
	}
	makeRequest(action, data, callback, submit_form);
}
function makeRequest(action, data, callback, submit_form) {
	setLoadingBar(true);
	var host = 'https://aligator-plus.herokuapp.com/';
	var url = host + action;

	var request_params = {
		method: 'POST',
		headers: {
  		'Content-type': 'multipart/form-data; charset=UTF-8'
 		},
		body: data
	};
	fetch(url, request_params)
		.then(response => response.json())
		.then(response => {
			callback(submit_form, response);
			setLoadingBar(false);
		})
		.catch(response => {
			console.error(response);
			setLoadingBar(false);
		});
}
function signUpResult(submit_form, response) {
	if (response.status === 200) {
		var help_message = submit_form.getElementsByClassName('form_help_message')[0];
		help_message.classList.remove('error_msg');
		help_message.innerText = 'Congratulations, now You can log in! :)';
		help_message.classList.add('success_msg');

		var login_button = document.getElementById('form_login_button');
		setTimeout(() => {
			login_button.click();
			setTimeout(() => document.getElementsByClassName('login-form')[0].reset(), 300)
		}, 1000);
	}
	else {
		resultError(submit_form, response);
	}
}
function logInResult(submit_form, response) {
	if (response.status === 200) {
		var help_message = submit_form.getElementsByClassName('form_help_message')[0];
		help_message.classList.remove('error_msg');
		help_message.innerText = 'Welcome!';
		help_message.classList.add('success_msg');

		var token = response.response.remember_token;
		if (token) {
			localStorage.setItem('aligator-plus_token', JSON.stringify(token));
		}
		setTimeout(() => window.location.href = "main_page.html", 1000);
	}
	else {
		resultError(submit_form, response);
	}
}
function resultError(submit_form, response) {
	var help_message = submit_form.getElementsByClassName('form_help_message')[0];
	if (response.response['errorMessages']) {
		help_message.classList.remove('success_msg');
		help_message.innerText = response.response['errorMessages'][0];
		help_message.classList.add('error_msg');
	}
}
function setLoadingBar(display_bool) {
	var visibility = display_bool ? 'visible' : 'hidden';
	var loading_bar_arr = document.getElementsByClassName('loading_bar');
	for (var i = 0; i < loading_bar_arr.length; i++) {
		loading_bar_arr[i].style.visibility = visibility;
	}
}

window.onload = function () {
	document.getElementById('form_registration_button').addEventListener('click', changeForm);

	document.getElementsByClassName('login-form')[0].addEventListener('submit', (event) => {
		event.preventDefault();
		submitForm();
	});
}